FROM node:16.17.0

COPY package.json yarn.lock ./

RUN yarn install

COPY . ./

CMD yarn start:dev ping
