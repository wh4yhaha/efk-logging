import { Controller, Get } from '@nestjs/common';
import { RequestAsyncLocalStorage } from './request.async-local-storage';
import { JsonLogger } from '@logger/json-logger';
import { PongService } from './pong/pong.service';

@Controller('/')
export class AppController {
  constructor(
    private readonly requestAsyncLocalStorage: RequestAsyncLocalStorage,
    private readonly logger: JsonLogger,
    private readonly pongService: PongService,
  ) {}

  @Get('/ping/grpc')
  async pingViaGrpc() {
    const { requestId } = this.requestAsyncLocalStorage.getStore();

    this.logger.debug(
      {
        requestId,
        message: 'PING REQUEST INITIATED',
      },
      AppController.name + '.pingViaGrpc',
    );

    const response = await this.pongService.pongViaGrpc(
      { message: 'Ping' },
      requestId,
    );

    this.logger.debug(
      {
        requestId,
        message: 'PING REQUEST ENDED',
        data: response,
      },
      AppController.name + '.pingViaGrpc',
    );

    return response;
  }

  @Get('/ping/rmq')
  async pingViaRmq() {
    const { requestId } = this.requestAsyncLocalStorage.getStore();

    this.logger.debug(
      {
        requestId,
        message: 'PING REQUEST INITIATED',
      },
      AppController.name + '.pingViaRmq',
    );

    await this.pongService.pongViaRmq({ message: 'Ping' }, requestId);
  }
}
