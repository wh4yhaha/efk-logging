import { AsyncLocalStorage } from 'async_hooks';
import { Provider } from '@nestjs/common';

export interface RequestStore {
  requestId: string;
}

export class RequestAsyncLocalStorage extends AsyncLocalStorage<RequestStore> {}

export const RequestAsyncLocalStorageProvider: Provider = {
  provide: RequestAsyncLocalStorage,
  useValue: new RequestAsyncLocalStorage(),
};
