import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { JsonLogger } from '@logger/json-logger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { logger: new JsonLogger() });
  await app.listen(3000);
}
bootstrap();
