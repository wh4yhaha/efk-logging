import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PongService } from './pong.service';
import { RmqModule } from '@rmq/rmq';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'PONG_GRPC_PACKAGE',
        transport: Transport.GRPC,
        options: {
          package: 'pong',
          protoPath: './libs/grpc/src/pong/pong.proto',
          url: 'dns:///pong_service:5000',
        },
      },
    ]),
    RmqModule,
  ],
  providers: [PongService],
  exports: [PongService],
})
export class PongModule {}
