import { Inject, Injectable } from '@nestjs/common';
import { ClientGrpc, RmqRecordBuilder } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { PingPongMessage, PongGrpcService } from '@grpc/grpc';
import { Metadata } from '@grpc/grpc-js';
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq';

@Injectable()
export class PongService {
  private pongGrpcService: PongGrpcService;

  constructor(
    @Inject('PONG_GRPC_PACKAGE') private readonly grpcClient: ClientGrpc,
    private readonly rmqClient: AmqpConnection,
  ) {}

  onModuleInit() {
    this.pongGrpcService =
      this.grpcClient.getService<PongGrpcService>('PongService');
  }

  async pongViaGrpc(
    message: PingPongMessage,
    requestId: string,
  ): Promise<{ message: string }> {
    const metadata = new Metadata();
    metadata.add('x-request-id', requestId);

    const observableResponse = this.pongGrpcService.pong(message, metadata);

    return lastValueFrom(observableResponse);
  }

  async pongViaRmq(message: PingPongMessage, requestId: string): Promise<void> {
    const data = new RmqRecordBuilder(message)
      .setOptions({
        headers: {
          ['x-request-id']: requestId,
        },
      })
      .build();

    await this.rmqClient.publish('ping-pong.exchange', 'pong', data);
  }
}
