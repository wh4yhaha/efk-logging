import { MiddlewareConsumer, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { PongModule } from './pong/pong.module';
import { JsonLoggerModule } from '@logger/json-logger';
import * as crypto from 'crypto';
import {
  RequestAsyncLocalStorage,
  RequestAsyncLocalStorageProvider,
  RequestStore,
} from './request.async-local-storage';

@Module({
  imports: [JsonLoggerModule, PongModule],
  controllers: [AppController],
  providers: [RequestAsyncLocalStorageProvider],
})
export class AppModule {
  constructor(
    private readonly requestAsyncLocalStorage: RequestAsyncLocalStorage,
  ) {}

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply((req, res, next) => {
        const store: RequestStore = {
          requestId: req.headers['x-request-id'] ?? crypto.randomUUID(),
        };

        this.requestAsyncLocalStorage.run(store, () => next());
      })
      .forRoutes('*');
  }
}
