import { Reflector } from '@nestjs/core';
import { Channel, ConsumeMessage } from 'amqplib';
import {
  RABBIT_HANDLER,
  RabbitHandlerConfig,
} from '@golevelup/nestjs-rabbitmq';

export const RMQ_REDELIVERED_COUNT_HEADER = 'x-redelivered-count';
export const RMQ_DELAY_HEADER = 'x-delay';

interface RedeliverRmqErrorHandlerOptions {
  deadLetterQueue: string;
  maxRetries: number;
  delay: number | number[];
}

export const redeliverRmqErrorHandler =
  (handler, options: RedeliverRmqErrorHandlerOptions) =>
  async (channel: Channel, msg: ConsumeMessage) => {
    const reflector = new Reflector();

    const {
      exchange,
      routingKey,
    }: Omit<RabbitHandlerConfig, 'type'> & { routingKey: string } =
      reflector.get(RABBIT_HANDLER, handler);

    let retryCount = msg.properties.headers[RMQ_REDELIVERED_COUNT_HEADER] ?? 0;

    channel.nack(msg, false, false);

    if (retryCount < options.maxRetries) {
      const delay = Array.isArray(options.delay)
        ? options.delay[retryCount]
        : options.delay;

      retryCount++;

      channel.publish(exchange, routingKey, msg.content, {
        headers: {
          ...msg.properties.headers,
          [RMQ_REDELIVERED_COUNT_HEADER]: retryCount,
          [RMQ_DELAY_HEADER]: delay,
        },
      });
    } else {
      channel.sendToQueue(options.deadLetterQueue, msg.content, {
        headers: msg.properties.headers,
      });
    }
  };
