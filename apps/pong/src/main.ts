import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';
import { JsonLogger } from '@logger/json-logger';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    logger: new JsonLogger(),
    transport: Transport.GRPC,
    options: {
      package: 'pong',
      protoPath: './libs/grpc/src/pong/pong.proto',
      url: '0.0.0.0:5000',
    },
  });

  await app.listen();
}
bootstrap();
