import { Injectable } from '@nestjs/common';
import { JsonLogger } from '@logger/json-logger';
import { AppService } from './app.service';
import { RabbitSubscribe } from '@golevelup/nestjs-rabbitmq';
import { redeliverRmqErrorHandler } from './redeliver.rmq-error-handler';

@Injectable()
export class AppRmqService {
  constructor(
    private readonly appService: AppService,
    private readonly logger: JsonLogger,
  ) {}

  @RabbitSubscribe({
    exchange: 'ping-pong.exchange',
    routingKey: 'pong',
    queue: 'pong.queue',
    createQueueIfNotExists: true,
    errorHandler: redeliverRmqErrorHandler(AppRmqService.prototype.pong, {
      deadLetterQueue: 'pong.queue.dlq',
      delay: [10 * 1000, 20 * 1000, 30 * 1000, 40 * 1000, 50 * 1000],
      maxRetries: 5,
    }),
  })
  public async pong(msg: any) {
    const requestId = msg.options.headers['x-request-id'];
    const data = msg.data;
    throw new Error();

    this.logger.debug({ data, requestId }, AppRmqService.name);
  }
}
