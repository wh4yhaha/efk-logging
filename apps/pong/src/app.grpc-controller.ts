import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { GrpcMethod } from '@nestjs/microservices';
import { Metadata } from '@grpc/grpc-js';
import { JsonLogger } from '@logger/json-logger';
import { PingPongMessage } from '@grpc/grpc';

@Controller()
export class AppGrpcController {
  constructor(
    private readonly appService: AppService,
    private readonly logger: JsonLogger,
  ) {}

  @GrpcMethod('PongService', 'Pong')
  pong(message: PingPongMessage, metadata: Metadata): PingPongMessage {
    const meta = metadata.getMap();
    this.logger.debug(
      {
        incomingMessage: message,
        requestId: meta['x-request-id'],
      },
      AppGrpcController.name,
    );

    return this.appService.pong();
  }
}
