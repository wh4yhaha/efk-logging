import { Module } from '@nestjs/common';
import { AppGrpcController } from './app.grpc-controller';
import { AppService } from './app.service';
import { JsonLoggerModule } from '@logger/json-logger';
import { AppRmqService } from './app.rmq-service';
import { RmqModule } from '@rmq/rmq';

@Module({
  imports: [JsonLoggerModule, RmqModule],
  controllers: [AppGrpcController],
  providers: [AppService, AppRmqService],
})
export class AppModule {}
