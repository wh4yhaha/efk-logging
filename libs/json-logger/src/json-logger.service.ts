import { LoggerService } from '@nestjs/common';

export type LogMessage<T> = T | object;

export enum LogLevel {
  LOG = 'LOG',
  WARN = 'WARN',
  ERROR = 'ERROR',
  DEBUG = 'DEBUG',
  VERBOSE = 'VERBOSE',
}

interface WriteLogOptions<Message> {
  message: LogMessage<Message>;
  level: LogLevel;
  context?: string;
  errorTrace?: string;
}

export class JsonLogger implements LoggerService {
  private write<T>(options: WriteLogOptions<T>) {
    let log = options.message;

    if (typeof log !== 'object') {
      log = { message: options.message };
    }

    if (options.level === LogLevel.ERROR) {
      Object.assign(log, { errorTrace: options.errorTrace });
    }

    console.log(
      JSON.stringify({
        '@level': options.level,
        context: options.context,
        ...log,
      }),
    );
  }

  public log<T>(message: LogMessage<T>, context?: string) {
    this.write({ message, level: LogLevel.LOG, context });
  }

  public error<T>(message: LogMessage<T>, trace: string, context?: string) {
    this.write({ message, level: LogLevel.ERROR, context, errorTrace: trace });
  }

  public warn<T>(message: LogMessage<T>, context?: string) {
    this.write({ message, level: LogLevel.WARN, context });
  }

  public debug<T>(message: LogMessage<T>, context?: string) {
    this.write({ message, level: LogLevel.DEBUG, context });
  }

  public verbose<T>(message: LogMessage<T>, context?: string) {
    this.write({ message, level: LogLevel.VERBOSE, context });
  }
}
