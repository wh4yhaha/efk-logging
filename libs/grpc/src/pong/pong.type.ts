import { Observable } from 'rxjs';
import { Metadata } from '@grpc/grpc-js';

export interface PongGrpcService {
  pong(
    message: PingPongMessage,
    metadata?: Metadata,
  ): Observable<{ message: string }>;
}

export interface PingPongMessage {
  message: string;
}
