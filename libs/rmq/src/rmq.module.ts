import { Module } from '@nestjs/common';
import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq';

@Module({
  imports: [
    RabbitMQModule.forRoot(RabbitMQModule, {
      exchanges: [
        {
          name: 'ping-pong.exchange',
          type: 'x-delayed-message',
          options: { arguments: { 'x-delayed-type': 'direct' } },
          createExchangeIfNotExists: true,
        },
      ],
      connectionInitOptions: {
        wait: false,
      },
      uri: 'amqp://rabbit:root@rabbitmq:5672',
    }),
  ],
  exports: [RabbitMQModule],
})
export class RmqModule {}
